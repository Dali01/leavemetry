package dali.hmida.leavemetry.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import dali.hmida.leavemetry.R
import dali.hmida.leavemetry.databinding.FragmentSignUp2Binding
import dali.hmida.leavemetry.databinding.FragmentSignUp3Binding


class SignUp3Fragment : Fragment() {

    private var _binding : FragmentSignUp3Binding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSignUp3Binding.inflate(inflater, container, false)

        val departments = resources.getStringArray(R.array.department)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, departments)
        binding.autoCompleteDepartment.setAdapter(arrayAdapter)




        return binding.root
    }


}