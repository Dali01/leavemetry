package dali.hmida.leavemetry.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import dali.hmida.leavemetry.R
import dali.hmida.leavemetry.databinding.FragmentResetPasswordBinding
import dali.hmida.leavemetry.databinding.FragmentSignUp2Binding

class SignUp2Fragment : Fragment() {

    private var _binding : FragmentSignUp2Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSignUp2Binding.inflate(inflater, container, false)

        val genders = resources.getStringArray(R.array.gender)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, genders)
        binding.autoCompleteTextView.setAdapter(arrayAdapter)


        return binding.root
    }


}